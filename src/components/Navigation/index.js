import React from 'react';
import './style.css';

function Navigation() {
  return (
    <ul className='nav-container'>
        <li className='nav-ele'>step1</li>
        <li className='nav-ele'>step2</li>
        <li className='nav-ele'>step3</li>
        <li className='nav-ele'>step4</li>
    </ul>
  )
}

export default Navigation