import React from 'react';
import './style.css';
import {useSelector, useDispatch} from 'react-redux';
import {changeFirstName, changeLastName, changePhoneNumber, changeMail, changeCountry, changeCity} from '../../store/userInfoSlice';

function UserPersonalInfo() {
    const {personalInfo} = useSelector(state => state.userInfo)
    const dispatch = useDispatch();
    const {firstName, lastName, phoneNumber, mail, country, city} = personalInfo

    const handleFirstName = event => dispatch(changeFirstName(event.target.value))
    const handleLastName = event => dispatch(changeLastName(event.target.value))
    const handlePhoneNumber = event => dispatch(changePhoneNumber(event.target.value))
    const handleMail = event => dispatch(changeMail(event.target.value))
    const handleCountry = event => dispatch(changeCountry(event.target.value))
    const handleCity = event => dispatch(changeCity(event.target.value))
    
  return (
    <div>
                <div className='hand-gesture-container'>
                    <img src='https://res.cloudinary.com/db5furjnj/image/upload/v1652340885/tap_xzaqlc.png' alt='show-hand-gesture' className='hand-gesture' />
                </div>
                <div>
                    <h1 className='personal-info'>Your Presonal Information</h1>
                    <p className='description'>Enter your personal information to get closer to companies</p>
                    <form className='form-container'>
                        <div className='input-container'>
                            <label htmlFor='firstName' className='label'>First Name</label>
                            <input type='text' id='firstName'placeholder='Berktug' className='input' value={firstName} onChange={handleFirstName}/>
                        </div>
                        <div className='input-container'>
                            <label htmlFor='lastName' className='label'>Last Name</label>
                            <input type='text' id='lastName' placeholder='Mutlu' className='input' value={lastName} onChange={handleLastName}/>
                        </div>
                        <div className='input-container'>
                            <label htmlFor='phoneNumber' className='label'>Phone Number</label>
                            <input type='tel' id='phoneNumber' placeholder='+90 000 000 000' className='input' value={phoneNumber} onChange={handlePhoneNumber}/>
                        </div>
                        <div className='input-container'>
                            <label htmlFor='mail' className='label'>E-Mail Address</label>
                            <input type='email' id='mail' placeholder='berktugmutlu@gmail.com' className='input' value={mail} onChange={handleMail}/>
                        </div>
                        <div className='input-container'>
                            <label htmlFor='country' className='label'>Country</label>
                            <input type='text' id='country' placeholder='Turkey' className='input' value={country} onChange={handleCountry}/>
                        </div>
                        <div className='input-container'>
                            <label htmlFor='city' className='label'>City</label>
                            <input type='text' id='city' placeholder='Izmir' className='input' value={city} onChange={handleCity}/>
                        </div>
                    </form>
                   
                </div>
        </div>
  )
}

export default UserPersonalInfo