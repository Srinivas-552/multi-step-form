import React from 'react'

function UserPhoto() {
  return (
    <div>
        <div className='hand-gesture-container'>
            <img src='https://res.cloudinary.com/db5furjnj/image/upload/v1652340885/tap_xzaqlc.png' alt='show-hand-gesture' className='hand-gesture' />
        </div>
        <div>
            <h1 className='personal-info'>User Photo</h1>
            <p className='description'>Upload your profile picture and show yourself.</p>
        </div>
        <div>
            
        </div>
    </div>
  )
}

export default UserPhoto