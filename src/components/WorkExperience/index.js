import React from 'react';
import './style.css';

function WorkExperienceInfo() {
  return (
    <div>
        <div className='hand-gesture-container'>
            <img src='https://res.cloudinary.com/db5furjnj/image/upload/v1652340885/tap_xzaqlc.png' alt='show-hand-gesture' className='hand-gesture' />
        </div>
        <div>
            <h1 className='personal-info'>Work Experiences</h1>
            <p className='description'>Can you talk about your past work experience?</p>
            <form id='form-container'>
                <div id='experience-info-container'>
                    <div className='input-container'>
                        <label htmlFor='experience1' className='label'>Experience 1</label>
                        <input type='search' id='experience1' placeholder='' className='input' />
                    </div>
                    <div className='input-container'>
                        <label htmlFor='position1' className='label'>Position</label>
                        <input type='text' id='position1' placeholder='' className='input' />
                    </div>
                </div>
                <div id='experience-info-container'>
                    <div className='input-container'>
                        <label htmlFor='experience2' className='label'>Experience 2</label>
                        <input type='search' id='experience2' placeholder='' className='input' />
                    </div>
                    <div className='input-container'>
                        <label htmlFor='position1' className='label'>Position</label>
                        <input type='text' id='position1' placeholder='' className='input' />
                    </div>
                </div>
                <div className='input-container'>
                    <label htmlFor='addNewSchool' className='label'>Experience 3</label>
                    <input type='button' id='addNewSchool' placeholder='' value='Add New Experience' className='input'/>
                </div>
            </form>      
        </div>
    </div>
  )
}

export default WorkExperienceInfo