import React from 'react';
import './style.css';

function EducationInfo() {
  return (
    <div id='education-bg-container'>
        <div className='hand-gesture-container'>
            <img src='https://res.cloudinary.com/db5furjnj/image/upload/v1652340885/tap_xzaqlc.png' alt='show-hand-gesture' className='hand-gesture' />
        </div>
        <div>
            <h1 className='personal-info'>Education</h1>
            <p className='description'>Inform companies about your education life</p>
            <form className='school-form-container'>
                <div className='school-input-container'>
                    <label htmlFor='secondarySchool' className='label'>School</label>
                    <input type='search' id='secondarySchool' placeholder='School' className='input' />
                </div>
                <div className='school-input-container'>
                    <label htmlFor='seniorSecondary' className='label'>School</label>
                    <input type='search' id='seniorSecondary' placeholder='College' className='input' />
                </div>
                <div className='school-input-container'>
                    <label htmlFor='addNewSchool' className='label'>School</label>
                    <input type='button' id='addNewSchool' placeholder='' value='Add New School' className='input'/>
                </div>
            </form>      
        </div>
    </div>
  )
}

export default EducationInfo