import React from 'react';
import './style.css';
import {useSelector, useDispatch} from 'react-redux';
import {changeNextPage, changePreviousPage} from '../../store/userInfoSlice';
import UserPersonalInfo from '../UserInfo';
import EducationInfo from '../EducationInfo';
import WorkExperienceInfo from '../WorkExperience';
import UserPhoto from '../UserPhoto';

function Form() {
    const {step} = useSelector(state => state.userInfo)
    const dispatch = useDispatch();

    const handleNextPage = () => {
        dispatch(changeNextPage())
    }

    const handlePreviousPage = () => {
        dispatch(changePreviousPage())
    }

    const renderForm = () => {
        switch(step){
            case 1:
                return <UserPersonalInfo />
            case 2:
                return <EducationInfo />
            case 3:
                return <WorkExperienceInfo />
            case 4:
                return <UserPhoto />
            default:
                return <UserPersonalInfo />
        }
    }
    let mystye1={},mystye2={},mystye3={},mystye4={};
    const changeValue =() => {
        switch(step) {
            case 1: {
                mystye1 = {opacity : 1}
                return 'Enter your personal information to get closer to companies.';
            }
            case 2:  {
                mystye2 = {opacity : 1}
                return 'Get to know better by adding your diploma, certificate and education life.';
            }
            case 3:  {
                mystye3 = {opacity : 1}
                return 'Help companies get to know you better by telling them about your past experiences.';
            }
            case 4:  {
                mystye4 = {opacity : 1} 
                return 'Add your profile picture and let companies find your past.';
            }
            default: return;
        }
    }

    
  return (
    <div>
        <div className='personal-info-bg-container'>
            <div className='step1-container'>
                <img src='indeed-logo.svg' alt='indeed-logo' className='indeed-logo' />
                <div className='page-info' >
                    <h2 className='step'>Step {step}</h2>
                    <p className='info'>{changeValue()}</p>
                    <div className='step-page-info' style={mystye1}>
                        <div className='page'>1</div>
                        <p className='info'>Personal Information</p>
                    </div>
                    <hr className='hr-line'/>
                    <div className='step-page-info' style={mystye2}>
                        <div className='page'>2</div>
                        <p className='info'>Education</p>
                    </div>
                    <hr className='hr-line'/>
                    <div className='step-page-info' style={mystye3}>
                        <div className='page'>3</div>
                        <p className='info'>Work Experiences</p>
                    </div>
                    <hr className='hr-line'/>
                    <div className='step-page-info' style={mystye4}>
                        <div className='page'>4</div>
                        <p className='info'>User Photo</p>
                    </div>
                    
                </div>
            </div>
            <div className='form-info-bg-container'>
                {renderForm()}
                <div className='btn-container'>
                    <button className='back-btn' disabled={step===1} onClick={handlePreviousPage}>Back</button>
                    <button className='next-step-btn' disabled={step===4} onClick={handleNextPage}>Next Step</button>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Form