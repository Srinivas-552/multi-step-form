import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    step: 1,
    personalInfo: {
        firstName: '',
        lastName: '',
        phoneNumber: '',
        mail: '',
        country: '',
        city: ''
    }
  }

export const userInfoSlice = createSlice({
  name: 'userInfo',
  initialState,
  reducers: {
    changeFirstName: (state, action) => {
        state.personalInfo.firstName = action.payload
    },
    changeLastName: (state, action) => {
        state.personalInfo.lastName = action.payload
    },
    changePhoneNumber: (state, action) => {
        state.personalInfo.phoneNumber = action.payload
    },
    changeMail: (state, action) => {
        state.personalInfo.mail = action.payload
    },
    changeCountry: (state, action) => {
        state.personalInfo.country = action.payload
    },
    changeCity: (state, action) => {
        state.personalInfo.city = action.payload
    },
    changeNextPage: (state) => {
        if(state.step<4){
            state.step += 1
        }
    },
    changePreviousPage: (state) =>{
        if(state.step>1){
            state.step -= 1
        }
    }
  }
})

export const { changeFirstName, changeLastName, changePhoneNumber, changeMail, changeCountry, changeCity, changeNextPage, changePreviousPage} = userInfoSlice.actions

export default userInfoSlice.reducer